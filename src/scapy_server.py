#!/usr/bin/python
from argparse import ArgumentParser,RawTextHelpFormatter
from scapy.all import TCP, IP, send, sniff,sr1
from threading import Thread
import logging
from termcolor import colored,cprint

# Interacts with a client by going through the three-way handshake.

port = 1060
host = "127.0.0.1"

log = logging.getLogger('server')
log.setLevel(logging.INFO)


prn = lambda x: x.summary

class Server(object):
	def __init__(self, host, port, timeout):
		self.host = host
		self.port = port
		self.timeout = timeout

	def handle_client(self):
		client_ip, client_port, seq_number, ack_number = self.open_connection()
#		seq_number, ack_number = self.get_msg(client_ip, client_port, seq_number, ack_number)		
		seq_number, ack_number = self.http_get(client_ip, client_port, seq_number, ack_number)
		self.close_connection(client_ip, client_port, seq_number, ack_number)

# 	def get_msg(self, client_ip, client_port, seq_number, ack_number):
# 		connected_user_filter = "tcp and port {0}".format(client_port)
# 		msg = sniff(filter=connected_user_filter,count=1,prn=prn)
# 		print "got msg"
# 		ack_number=ack_number+1  # not sure that +1 is good. 
# #		AckNr=AckNr+len(GEThttp[0].load)
# 		seq_number=seq_number+1

# 		ip=IP(src=self.host, dst=client_ip)  #dst=package[0].dst)  #dst="127.0.0.1")
# 		data1=TCP(sport=self.port, dport=client_port, flags="PA", seq=seq_number, ack=ack_number, options=[('MSS', 1460)])
# 		ackdata1=sr1(ip/data1/"OK")
# 		seq_number=ackdata1.ack
# 		return seq_number, ack_number


	def open_connection(self):
		# print self.timeout
		packages_filter = "tcp and host {0} and port {1}".format(self.host, self.port)
		package=sniff(count=1,prn=prn , filter=packages_filter)
		# if package:
		log.info("received SYN")

		client_port=package[0][TCP].sport
		seq_number=package[0][TCP].seq
		ack_number=package[0][TCP].seq+1

		# Generating the IP layer:
		# client_ip = "127.0.0.1" is hardcode. must somehow get this value from packag
		client_ip = package[0][IP].src
		#client_ip = "127.0.0.1" 
		
		ip_layer=IP(src=self.host, dst=client_ip)  #dst=package[0].dst)  #dst="127.0.0.1")
		# Generating TCP_SYNACK:
		TCP_SYNACK=TCP(
			sport=self.port, dport=client_port, 
			flags="SA", seq=seq_number, 
			ack=ack_number, options=[('MSS', 1460)]
			)		
		#send SYNACK to client AND receive ACK.
		ANSWER=sr1(ip_layer/TCP_SYNACK, timeout=self.timeout)

		# print ANSWER
		print 'Connection established'

		return client_ip, client_port, seq_number, ack_number


	# Generate RST-ACK packet
	def close_connection(self, client_ip, client_port, seq_number, ack_number):
		ip=IP(src=self.host, dst=client_ip)
		Bye=TCP(
			sport=self.port, dport=client_port, 
			flags="FA", seq=seq_number, 
			ack=ack_number, options=[('MSS', 1460)]
			)
		send(ip/Bye)

	def run_server(self):
# 	threads_number = 10
# while True:

# 	for i in xrange(0,threads_number):
# 		thread=Thread(target=handle_client)
# 		# thread.setDaemon(True)
# 		thread.start()
# 		thread.join()

		while True:
			try:
				self.handle_client()
			except KeyboardInterrupt:
				print "Server stopping"
				exit(1)


	def http_get(self, client_ip, client_port, SeqNr, AckNr):
		# prn = lambda x:x.sprintf("{IP:%IP.src%: %TCP.dport%")
		#HOW DO FILTER TO GET  MSG ONLY FROM CONNECTED CLIENT
		print client_ip, client_port
		# client_filter = "ip src host {0} and ip dst host{1} and tcp sport {2} and tcp dport {3}".format(client_ip, self.host, client_port, self.port)
		client_filter = "tcp dst port {0}".format(self.port)
		GEThttp = sniff(filter=client_filter, count=1, prn=prn)#prn)
		print "get http"

	
		AckNr=AckNr+1  #len(GEThttp[0].load)
		SeqNr=SeqNr+1

		# Print the GET request
		# (Sanity check: size of data should be greater than 1.)
		# if len(GEThttp[0].load)>1: print GEThttp[0].load

		# Generate custom http file content.
		html1="HTTP/1.1 200 OK\x0d\x0aDate: Wed, 29 Sep 2010 20:19:05 GMT\x0d\x0aServer: Testserver\x0d\x0aConnection: Keep-Alive\x0d\x0aContent-Type: text/html; charset=UTF-8\x0d\x0aContent-Length: 291\x0d\x0a\x0d\x0a<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\"><html><head><title>Testserver</title></head><body bgcolor=\"black\" text=\"white\" link=\"blue\" vlink=\"purple\" alink=\"red\"><p><font face=\"Courier\" color=\"blue\">-Welcome to test server-------------------------------</font></p></body></html>"

		# Generate TCP data
		data1=TCP(sport=self.port, dport=client_port, flags="PA", seq=SeqNr, ack=AckNr, options=[('MSS', 1460)])

		# Construct whole network packet, send it and fetch the returning ack.
		#send html ok and receive ack

		print "sending reply html"
		ip = IP(src=self.host, dst=client_ip)
		ackdata1=sr1(ip/data1/html1)
		# Store new sequence number.
		SeqNr=ackdata1.ack
		return SeqNr, AckNr




	# Capture next TCP packets. (contains http GET request)
	# GEThttp = sniff(filter="tcp and port {0}".format(port),count=1,prn=lambda x:x.sprintf("{IP:%IP.src%: %TCP.dport%}"))
	
	# The End

def parse_arguments():
	# title = "Server for attack"
	parser = ArgumentParser(
        usage='./%(prog)s -d [ip| domain] -p [port] -t [timeout] -T[threads number]',
        formatter_class=RawTextHelpFormatter,
        prog='server',
        # description=cprint(title,'white',attrs=['bold']),
        epilog='''
Example:
    ./%(prog)s -h 127.0.0.1 -p 1060 -T 10 -t 1
'''
)
	options = parser.add_argument_group('options','')
	options.add_argument('-d',metavar='<ip|domain>',default='127.0.0.1',help='Specify server ip or domain name')
	options.add_argument('-t',metavar='<int>',default=None,help='Set timeout for socket. Default no timeout')
	# options.add_argument('-T',metavar='<int>',default=1,help='Set threads number for connection (default = 1)')
	options.add_argument('-p',metavar='<int>',default=1060,help='Specify port target (default = 1060)')
	# options.add_argument('-s',metavar='<int>',default=100,help='Set sleep time for reconnection')
	# options.add_argument('-i',metavar='<ip address>',default=False,help='Specify spoofed ip unless use fake ip')
	
	args = parser.parse_args()
	args.t = int(args.t)
	# print type(args.t)

	# print args.t
	# if args.d == False:
	# 	parser.print_help()
	# 	sys.exit()
	# if args.d:
	# 	check_tgt(args)
	return args.d,args.p, args.t #, args.T


def main():
    host, port, timeout = parse_arguments()  #threads_number
    server = Server(host, port, timeout)
    server.run_server()


if __name__=='__main__':
	main()