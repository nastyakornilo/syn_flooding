# #!/usr/bin/python
# from scapy.all import *
# # scapy
# ip=IP(dst="192.168.1.1")
# port=1061
# SYN=ip/TCP(sport=port, dport=1060, flags="S", seq=42)
# print 'SYN created',SYN
# SYNACK=sr1(SYN)
# print 'SYNACK received ', SYNACK
# ACK=ip/TCP(sport=SYNACK.dport, dport=1060, flags="A", seq=SYNACK.ack, ack=SYNACK.seq + 1)
# send(ACK)
# print "ACK was send", ACK
import logging
from scapy.all import *


log= logging.getLogger("legatimate client")
log.setLevel(logging.INFO)

client_port=1061
server_port = 1060
server_host = "127.0.0.1"
client_ip = "127.0.0.1"

 
print "TCP Handshake. Legatimate client"
 
# Set up target IP
ip=IP(dst=server_host)
 
#Create SYN packet

def connect():
	SYN=ip/TCP(sport=client_port, dport=server_port, flags="S", seq=42)
	 
	# Send SYN and receive SYN,ACK
	log.info("Sending SYN packet")
	SYNACK=sr1(SYN)
	log.info("Receiving SYN,ACK packet")
	 
	# Create ACK packet
	ACK=ip/TCP(sport=client_port, dport=server_port, flags="A", seq=SYNACK.ack, ack=SYNACK.seq + 1)
	 
	# SEND our ACK packet
	log.info("Sending ACK packet")
	send(ACK)
	 
	print "\nConnection established!"

	seq = SYNACK[TCP].ack
	ack = seq + 1

	return seq, ack


# hello_msg = "Hello, server"
# ip = IP(dst=server_host, src=client_ip)

def http_get(seq, ack):
	tcp_data = TCP(sport=client_port, dport=server_port, seq=seq, ack=ack, flags='A')
	getStr = 'GET / HTTP/1.1\r\nHost: 127.0.0.1\r\n\r\n'
	request = ip/tcp_data/getStr
	
	print "sending request to server"
	reply = sr1(request)
	print "server reply: ", reply
	
	seq = reply[TCP].seq+1
	ACK=ip/TCP(sport=client_port, dport=server_port, flags="A", seq=seq, ack=seq + 1)
	send(ACK)


def main():
	#####first run
	# seq, ack = connect()

	###second run
	seq = 44
	ack = 45
	http_get(seq, ack)

main()


